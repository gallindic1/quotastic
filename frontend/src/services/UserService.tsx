import axios from 'axios';
import { LoginInterface, SignUpInterface } from '../interfaces/auth.interface';

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
};

const TOKEN_REFRESH_TIME = 60 * 1000 * 14; // 14 minutes

const createUser = async (data: SignUpInterface) => {
  return axios.post('/backend/users/signup', JSON.stringify(data), {
    headers: headers,
  })
      .then((res: any) => res)
      .catch((error: any) => error.response);
};

const login = async (data: LoginInterface) => {
  const userData = {
    username: data.email,
    password: data.password,
  };

  return axios.post('backend/users/login', JSON.stringify(userData), {
    headers: headers,
  })
      .then((res: any) => res)
      .catch((error: any) => error.response);
};

const userSignout = async (tokenKey: string) => removeAuthToken(tokenKey);

const authUser = async (token: string) => {
  return axios.get('backend/users/auth', {
    headers: {
      ...headers,
      'Authorization': `Bearer ${token}`,
    },
  })
      .then((res: any) => res)
      .catch((error: any) => error.response);
};

const refreshToken = (tokenKey: string) => {
  const token: string | null = getAuthToken(tokenKey);

  if (!token) {
    return;
  }

  const tokenPayload = extractTokenPayload(token);

  if (!tokenAlmostExpired(tokenPayload.exp)) {
    return;
  }

  axios.post('backend/users/refresh-token', JSON.stringify(tokenPayload), {
    headers: headers,
  })
      .then((res: any) => {
        const { data: { token } } = res;
        setAuthToken('token', token);
      });
};

const setAuthToken = (tokenKey: string, token: string) => {
  localStorage.setItem(tokenKey, token);
};

const getAuthToken = (tokenKey: string) => localStorage.getItem(tokenKey);

const removeAuthToken = (tokenKey: string) => localStorage.removeItem(tokenKey);

const extractTokenPayload = (token: string) => JSON.parse(atob(token.split('.')[1]));

const tokenAlmostExpired = (tokenExpiration: string) => {
  const expiration = new Date(tokenExpiration);
  const now = new Date();
  const minutes = 1000 * 60 * 14;

  return expiration.getTime() - now.getTime() < minutes;
};

export {
  TOKEN_REFRESH_TIME,
  createUser,
  login,
  authUser,
  getAuthToken,
  refreshToken,
  setAuthToken,
  removeAuthToken,
  userSignout,
};
