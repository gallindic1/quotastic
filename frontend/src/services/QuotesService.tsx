import axios from 'axios';
import { QuoteDataInferface } from '../interfaces/quote.interface';
import { getAuthToken } from './UserService';

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
};

const createQuote = async (data: QuoteDataInferface) => {
  return axios.post('backend/quotes/myquote', JSON.stringify(data), {
    headers: {
      ...headers,
      'Authorization': `Bearer ${getAuthToken('token')}`,
    },
  })
      .then((res: any) => res)
      .catch((error: any) => error.response);
};

const getRecentQuotes = async () => {
  return axios.get('backend/quotes/recent', {
    headers: headers,
  })
      .then((res: any) => res)
      .catch((error: any) => error.response);
};

export {
  createQuote,
  getRecentQuotes,
};
