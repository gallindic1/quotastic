import React from 'react';
import { useNavigate } from 'react-router-dom';
import { RegistrationForm } from '../../components/form/registration/RegistrationForm';
import './styles.scss';

export const Signup = () => {
  const navigate = useNavigate();

  return (
    <div className="signup page-container">
      <div className="signup-container">
        <h2 className="form-title">What is your <span className="primary-color">name</span></h2>
        <div className="form-description">
          Your name will appear on quotes and your public profle.
        </div>
        <div className="avatar"></div>
        <RegistrationForm />
        <div className="form-options">
          <div className="form-options-desc">Already have an account?</div>
          <div className="form-option" onClick={() => navigate('/login')}>Sign in</div>
        </div>
      </div>
    </div>
  );
};
