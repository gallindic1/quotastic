import React from 'react';
import { Hero } from '../../components/hero/Hero';
import { RecentQuotes } from '../../components/quote/recent-quotes/RecentQuotes';
import './styles.scss';

type LandingPageProps = {
  isUserLogged: boolean;
};

export const Landing = ({ isUserLogged }: LandingPageProps) => {
  const renderSecondaryTitle = () => {
    if (isUserLogged) {
      return null;
    }

    return (
      <h2 className="secondary-title">
        Explore the world of <br/><span className="primary-color">fantastic quotes</span>
      </h2>
    );
  };

  const renderLikedQuotes = () => {
    return (
      <div className="quotes-title-container">
        <h3>Most upvoted quotes</h3>
        <div className="description">
          Most upvoted quotes on the platform. Give a like to the ones you like to
          keep them saved in your profile.
        </div>
      </div>
    );
  };

  const renderRecentQuotes = () => {
    if (!isUserLogged) {
      return null;
    }

    return (
      <>
        <div className="quotes-title-container">
          <h3>Most recent quotes</h3>
          <div className="description">
            Recent quotes updates as soon user adds new quote. Go ahed
            show them that you seen the new quote and like the ones you
            like.
          </div>
        </div>
        <RecentQuotes />
      </>
    );
  };

  return (
    <div className="landing page-container">
      <Hero />
      <div className="quotes">
        { renderSecondaryTitle() }
        { renderLikedQuotes() }
        { renderRecentQuotes() }
      </div>
      <div className="top-vector-img"></div>
    </div>
  );
};
