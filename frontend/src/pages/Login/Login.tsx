import React from 'react';
import { LoginForm } from '../../components/form/login/LoginForm';
import './styles.scss';

export const Login = () => {
  return (
    <div className="login page-container">
      <div className="login-container">
        <h2 className="form-title">Welcome <span className="primary-color">back</span></h2>
        <div className="form-description">
          Thank you for coming back. Hope you have a good day and inspire others.
        </div>
        <LoginForm />
      </div>
    </div>
  );
};
