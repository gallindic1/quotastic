import { useEffect, RefObject } from 'react';

export const useOutsideAlerter = (
    ref: RefObject<HTMLDivElement>,
    clickEventHandler: ( event: Event ) => void,
) => {
  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);

    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, [ref]);

  const handleClickOutside = (event: Event) => {
    const { target } = event;
    if (ref.current && !ref.current.contains(target as HTMLButtonElement)) {
      clickEventHandler(event);
    }
  };
};
