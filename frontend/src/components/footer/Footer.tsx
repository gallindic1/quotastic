import React from 'react';
import './styles.scss';

export const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-container">
        <div className="footer-img"></div>
        <div className="footer-msg">
          All Rights Reserved | skillupmentor.com
        </div>
      </div>
    </div>
  );
};
