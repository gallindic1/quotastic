import React, { MouseEvent } from 'react';
import { useNavigate } from 'react-router-dom';
import { NavItem } from '../nav-item/NavItem';
import { Button } from '../../button/button';

type DefaultNavProps = {
  route: string;
};

export const DefaultNav = ({ route }: DefaultNavProps): JSX.Element => {
  const navigate = useNavigate();

  const renderLogInNavItems = () => (
    <NavItem>
      <Button type={'btn-primary'} clickEventHandler={(e: MouseEvent) => navigate('/signup')}>Sign Up</Button>
    </NavItem>
  );

  const renderSignUpNavItems = (): JSX.Element => (
    <NavItem>
      <Button type={'btn-default'} clickEventHandler={(e: MouseEvent) => navigate('/login')}>Login</Button>
    </NavItem>
  );

  const renderDefaultNavItems = (): JSX.Element => (
    <>
      <NavItem customClasses={'btn-margin-right'}>
        <Button type={'btn-primary'} clickEventHandler={(e: MouseEvent) => navigate('/signup')}>Sign Up</Button>
      </NavItem>
      <NavItem>
        <Button type={'btn-default'} clickEventHandler={(e: MouseEvent) => navigate('/login')}>Login</Button>
      </NavItem>
    </>
  );

  const handleCurrentNavItems = (): JSX.Element => {
    switch (route) {
      case '/login':
        return renderLogInNavItems();
      case '/signup':
        return renderSignUpNavItems();
      default:
        return renderDefaultNavItems();
    }
  };

  return handleCurrentNavItems();
};
