import React, { useState, useContext } from 'react';
import { userContext } from '../../../contexts/userContenxt';
import { NavItem } from '../nav-item/NavItem';
import { userSignout } from '../../../services/UserService';
import { QuoteModal } from '../../modal/quotes/QuoteModal';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

export const AuthNav = (): JSX.Element => {
  const navigate = useNavigate();
  const { setUser } = useContext(userContext);
  const [openQuoteModal, setOpenQuoteModal] = useState<boolean>(false);

  const signout = () => {
    userSignout('token');
    setUser(null);
    navigate('/');
    toast.success('Logged out successfully');
  };

  return (
    <div className="navbar-auth">
      <NavItem>Home</NavItem>
      <NavItem>Settings</NavItem>
      <NavItem handleItemClick={ signout }>Logout</NavItem>
      <NavItem customClasses={'nav-avatar-btn'}>
        <div className="nav-circle-btn user-avatar"></div>
      </NavItem>
      <NavItem handleItemClick={() => setOpenQuoteModal(true)}>
        <div className="nav-circle-btn add-quote-btn"></div>
      </NavItem>
      { openQuoteModal ?
        <QuoteModal closeModal={() => setOpenQuoteModal(false)}
          clickOutsideHandler={() => setOpenQuoteModal(false)} /> :
        null
      }
    </div>
  );
};
