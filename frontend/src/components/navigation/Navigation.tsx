import React, { MouseEvent } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { NavItem } from './nav-item/NavItem';
import { DesktopNav } from './desktop-nav/DesktopNav';
import './styles.scss';
import { DefaultNav } from './nav-views/DefaultNav';
import { AuthNav } from './nav-views/AuthNav';

type NavigationProps = {
  isUserLogged: boolean;
};

export const Navigation = ({ isUserLogged }: NavigationProps): JSX.Element => {
  const navigate = useNavigate();

  const handleNavigationItems = () => {
    if ( isUserLogged ) {
      return <AuthNav />;
    }

    const currentPath = useLocation().pathname;
    return <DefaultNav route={currentPath} />;
  };

  const renderNavbar = (): JSX.Element => (
    <DesktopNav>
      <div className="navbar-container">
        <NavItem customClasses='logo' handleItemClick={(e: MouseEvent) => navigate('/')}></NavItem>
        <div className="navbar-container-items">
          { handleNavigationItems() }
        </div>
      </div>
    </DesktopNav>
  );

  return (
    <div className="navbar">
      { renderNavbar() }
    </div>
  );
};
