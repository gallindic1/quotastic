import React, { ReactNode, MouseEvent } from 'react';
import '../styles.scss';

type NavItemProps = {
  handleItemClick?: (event: MouseEvent) => void,
  children?: ReactNode,
  customClasses?: string
}

export const NavItem = ({ handleItemClick, children, customClasses }: NavItemProps) => {
  return (
    <div className={`nav-item ${ customClasses }`} {...(handleItemClick && { onClick: handleItemClick })}>
      { children }
    </div>
  );
};
