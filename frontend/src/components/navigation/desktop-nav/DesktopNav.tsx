import React from 'react';

type DesktopNavProps = {
  children: JSX.Element | JSX.Element;
};

export const DesktopNav = ({ children }: DesktopNavProps): JSX.Element => {
  return (
    <div className="desktop-nav">
      { children }
    </div>
  );
};
