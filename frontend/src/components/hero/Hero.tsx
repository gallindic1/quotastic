import React, { MouseEvent } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button } from '../button/button';
import './styles.scss';

export const Hero = () => {
  const navigate = useNavigate();

  return (
    <div className="hero">
      <div className="hero-container">
        <div className="hero-container-text">
          <h1 className="page-title">Welcome <br/> to <span className="primary-color">Quotastic</span></h1>
          <div className="description">
            Quotastic is free online platform for you to explore the
              quips, quotes, and proverbs. Sign up and express yourself.
          </div>
          <Button type={'btn-primary'} clickEventHandler={(e: MouseEvent) => navigate('/signup')}>Sign up</Button>
        </div>
        <div className="quotes-block"></div>
      </div>
    </div>
  );
};
