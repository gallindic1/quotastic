import React, { useEffect, useState } from 'react';
import { getRecentQuotes } from '../../../services/QuotesService';
import { Button } from '../../button/button';
import { Quote } from '../Quote';
import Layout from 'react-masonry-list';
import { RecentQuotesData } from '../../../interfaces/quote.interface';

export const RecentQuotes = () => {
  const [recentQuotes, setRecentQuotes] = useState([]);

  const fetchRecentQuotes = async () => {
    const { status, data } = await getRecentQuotes();

    if (status !== 200) {
      return;
    }

    setRecentQuotes(data);
  };

  useEffect(() => {
    fetchRecentQuotes();
  }, []);

  const renderRecentQuotes = () => {
    return recentQuotes.map((quote: RecentQuotesData, idx: number) => {
      return <Quote key={idx} author={quote.author} quote={quote.quote} votes={0} />;
    });
  };

  return (
    <div className="recent-quotes">
      <Layout items={renderRecentQuotes()} className="masonry-layout" />
      <Button type={'btn-default'} clickEventHandler={() => console.log('to-do')}>Load More</Button>
    </div>
  );
};
