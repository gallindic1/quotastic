import React from 'react';
import { IoIosArrowUp, IoIosArrowDown } from 'react-icons/io';
import './styles.scss';

type QuoteProps = {
  author: string;
  votes: number;
  quote: string;
};

export const Quote = ({ author, votes, quote }: QuoteProps) => {
  return (
    <div className="quote">
      <div className="quote-card">
        <div className="votes-container">
          <IoIosArrowUp />
          { votes }
          <IoIosArrowDown />
        </div>
        <div className="quote-container">
          <div className="quote-container-message">
            { quote }
          </div>
          <div className="quote-container-author">
            <div className="author-avatar"></div>
            <div className="author">
              { author }
            </div>
          </div>
        </div>
      </div>
    </div>

  );
};
