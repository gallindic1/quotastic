import React, { MouseEvent, ReactNode } from 'react';
import './styles.scss';

type buttonProps = {
    clickEventHandler: ( event: MouseEvent ) => void,
    type: 'btn-primary' | 'btn-secondary' | 'btn-default',
    children?: ReactNode
}

export const Button = ({ clickEventHandler, type, children } : buttonProps) => {
  return (
    <div onClick={ clickEventHandler } className={`btn ${ type }`}>
      { children }
    </div>
  );
};
