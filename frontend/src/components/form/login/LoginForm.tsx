import React, { useState, useContext } from 'react';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { LoginInterface } from '../../../interfaces/auth.interface';
import { login, setAuthToken } from '../../../services/UserService';
import { Form } from '../form-elements/Form';
import { Input } from '../form-elements/Input';
import { Submit } from '../form-elements/Submit';
import { FormErrors } from '../form-elements/FormErrors';
import { validationRules } from './validationRules';
import '../styles.scss';
import { userContext } from '../../../contexts/userContenxt';

export const LoginForm = () => {
  const [formErrors, setFormErrors] = useState<string[]>([]);
  const { setUser } = useContext(userContext);
  const navigate = useNavigate();

  const onSubmit = async (formData: LoginInterface) => {
    const { status, data } = await login(formData);

    if (status !== 201) {
      toast.error('Something went wrong');
      setFormErrors([data.message]);

      return;
    }

    const { message, user, token } = data;

    setUser(user);
    setFormErrors([]);
    setAuthToken('token', token);

    toast.success(message);
    navigate('/');
  };

  return (
    <div className="form-container">
      {
        formErrors.length > 0 ?
        <FormErrors errors={ formErrors }/> : null
      }
      <Form<LoginInterface> onSubmit={onSubmit}>
        <div className="form-row">
          <div className="form-widget">
            <Input type={'email'} label={'Email'} name={'email'} rules={validationRules['email']}/>
          </div>
        </div>
        <div className="form-row">
          <div className="form-widget">
            <Input type={'password'} label={'Password'} name={'password'} rules={validationRules['password']}/>
          </div>
        </div>
        <div className="form-row">
          <div className="form-widget">
            <Submit value={'Login'} btnType={'btn-secondary'} />
          </div>
        </div>
      </Form>
    </div>
  );
};
