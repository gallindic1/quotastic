const validationRules = {
  'email': {
    required: 'You must enter your email',
  },
  'password': {
    required: 'You must enter your password',
  },
};

export {
  validationRules,
};
