import React from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';

type FormProps<FormValues> = {
  children: JSX.Element | JSX.Element[],
  onSubmit: SubmitHandler<FormValues>;
}

export const Form = <FormValues, > ({ children, onSubmit }: FormProps<FormValues>): JSX.Element => {
  const { handleSubmit, register, formState: { errors } } = useForm<FormValues>();

  const recursiveMapFormChildren = (children: JSX.Element | JSX.Element[]): JSX.Element[] => {
    return React.Children.map(children, (child) => {
      if (!React.isValidElement(child)) {
        return child;
      }

      // @ts-ignore
      if (child.props.children) {
        // @ts-ignore
        child = React.createElement(child.type, {
          ...{
            // @ts-ignore
            ...child.props,
          },
        },
        // @ts-ignore
        recursiveMapFormChildren(child.props.children));
      }

      return child.props.name ?
      React.createElement(child.type, {
        ...{
          ...child.props,
          key: child.props.name,
          errors,
          register,
        },
      }) :
      child;
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      { recursiveMapFormChildren(children) }
    </form>

  );
};
