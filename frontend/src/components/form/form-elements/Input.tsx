import React from 'react';
import { UseFormRegister, Path, RegisterOptions, DeepMap, FieldError } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

type FormElementProps<TFormValues> = {
  type: 'text' | 'password' | 'email';
  label: string;
  name: Path<TFormValues>;
  errors?: DeepMap<TFormValues, FieldError>
  rules?: RegisterOptions;
  register?: UseFormRegister<TFormValues>;
};

export const Input = <TFormValues, >(
  { type,
    label,
    name,
    errors,
    register,
    rules,
  }: FormElementProps<TFormValues>): JSX.Element => {
  return (
    <>
      <div className="form-label">
        { label }
      </div>
      <div className="form-control">
        <input
          className="input-control"
          type={ type }
          {...(register && register(name, rules))}
        />
        <ErrorMessage
          errors={errors}
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          name={name as any}
          render={({ message }) => (
            <span className="field-error">{ message }</span>
          )}
        />
      </div>
    </>
  );
};
