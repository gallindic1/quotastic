import React from 'react';

type FormErrorsProps = {
  errors: string[],
};

export const FormErrors = ({ errors }: FormErrorsProps) => {
  const renderFormErrors = () => {
    return errors.map((error: string, idx: number) => {
      return (
        <li key={idx}>{ error }</li>
      );
    });
  };

  return (
    <ul className="form-errors-container">
      { renderFormErrors() }
    </ul>
  );
};
