import React from 'react';

type FormElementProps = {
  value: string;
  btnType: 'btn-primary' | 'btn-secondary' | 'btn-default',
};

export const Submit = ({ value, btnType }: FormElementProps) => {
  return (
    <div className="form-control">
      <input className={`btn ${btnType}`} type="submit" value={ value } />
    </div>
  );
};
