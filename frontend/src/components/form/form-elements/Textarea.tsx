import React from 'react';
import { UseFormRegister, Path, RegisterOptions, DeepMap, FieldError } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

type FormElementProps<TFormValues> = {
  name: Path<TFormValues>;
  errors?: DeepMap<TFormValues, FieldError>
  rules?: RegisterOptions;
  register?: UseFormRegister<TFormValues>;
  customStyling?: string;
};

export const Textarea = <TFormValues, >(
  { name,
    errors,
    register,
    rules,
    customStyling,
  }: FormElementProps<TFormValues>): JSX.Element => {
  return (
    <>
      <div className="form-control">
        <textarea
          className={`input-control ${ customStyling }`}
          {...(register && register(name, rules))}
        ></textarea>
        <ErrorMessage
          errors={errors}
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          name={name as any}
          render={({ message }) => (
            <span className="field-error">{ message }</span>
          )}
        />
      </div>
    </>
  );
};
