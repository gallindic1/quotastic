import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Input } from '../form-elements/Input';
import { FormErrors } from '../form-elements/FormErrors';
import { createUser } from '../../../services/UserService';
import { SignUpInterface } from '../../../interfaces/auth.interface';
import { Form } from '../form-elements/Form';
import { Submit } from '../form-elements/Submit';
import { validationRules } from './validationRules';
import '../styles.scss';

export const RegistrationForm = () => {
  const [formErrors, setFormErrors] = useState<string[]>([]);
  const navigate = useNavigate();

  const onSubmit = async (formData: SignUpInterface) => {
    const { status, data } = await createUser(formData);

    if (status === 400) {
      toast.error('Something went wrong');
      setFormErrors(data.message);
      return;
    }

    toast.success(data.message);
    navigate('/login');
  };

  return (
    <div className="form-container">
      {
        formErrors.length > 0 ?
        <FormErrors errors={ formErrors }/> : null
      }
      <Form<SignUpInterface> onSubmit={ onSubmit }>
        <div className="form-row">
          <div className="form-widget">
            <Input name={'email'} label={'Email'} type={'email'} rules={validationRules['email']} />
          </div>
        </div>
        <div className="form-row">
          <div className="form-split-widget">
            <Input name={'first_name'} label={'First Name'} type={'text'} rules={validationRules['firstName']} />
          </div>
          <div className="form-split-widget">
            <Input name={'last_name'} label={'Last Name'} type={'text'} rules={validationRules['lastName']} />
          </div>
        </div>
        <div className="form-row">
          <div className="form-widget">
            <Input name={'password'} label={'Password'} type={'password'} rules={validationRules['password']} />
          </div>
        </div>
        <div className="form-row">
          <div className="form-widget">
            <Input name={'confirm_password'}
              label={'Confirm Password'} type={'password'} rules={validationRules['confirmPassword']} />
          </div>
        </div>
        <div className="form-row">
          <div className="form-widget">
            <Submit value={'Sign Up'} btnType={'btn-primary'} />
          </div>
        </div>
      </Form>
    </div>
  );
};
