const validationRules = {
  'email': {
    required: 'You must enter your email',
  },
  'firstName': {
    required: 'You must enter your first name',
  },
  'lastName': {
    required: 'You must enter your last name',
  },
  'password': {
    required: 'You must enter your password',
  },
  'confirmPassword': {
    required: 'You must confirm your password',
  },
};

export {
  validationRules,
};
