const validationRules = {
  'quote': {
    required: 'You must enter your quote!',
    minLength: {
      value: 5,
      message: 'Quote must consist of at least 5 characters!',
    },
    maxLength: {
      value: 280,
      message: 'Quote must not be longer than 280 characters!',
    },
  },
};

export {
  validationRules,
};
