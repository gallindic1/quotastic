import React, { useState } from 'react';
import { Form } from '../form-elements/Form';
import { FormErrors } from '../form-elements/FormErrors';
import { QuoteDataInferface } from '../../../interfaces/quote.interface';
import { Submit } from '../form-elements/Submit';
import { Textarea } from '../form-elements/Textarea';
import { validationRules } from './validationRules';
import { createQuote } from '../../../services/QuotesService';
import { toast } from 'react-toastify';
import '../styles.scss';

type QuoteFormProps = {
  onCancelClick: () => void;
};

export const AddQuoteForm = ({ onCancelClick }: QuoteFormProps) => {
  const [formErrors, setFormErrors] = useState<string[]>([]);

  const onSubmit = async (formData: QuoteDataInferface) => {
    const { status, data } = await createQuote(formData);

    if (status !== 201) {
      setFormErrors([data.message]);
      toast.error('Something went wrong');
      return;
    }

    setFormErrors([]);
    toast.success('Saved quote');
    onCancelClick();
  };

  return (
    <div className="form-container quote-form">
      {
        formErrors.length > 0 ?
        <FormErrors errors={ formErrors }/> : null
      }
      <Form<QuoteDataInferface> onSubmit={ onSubmit }>
        <div className="form-row">
          <div className="form-widget">
            <Textarea name={'quote'} customStyling={'nonresizable quote-textarea'} rules={ validationRules['quote'] } />
          </div>
        </div>
        <div className="form-row">
          <div className="form-widget quote-form-btns">
            <Submit value={'Submit'} btnType={'btn-primary'} />
            <div className="cancel-btn" onClick={onCancelClick}>Cancel</div>
          </div>
        </div>
      </Form>
    </div>
  );
};
