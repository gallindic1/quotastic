import React from 'react';
import { Modal } from '../modal-elements/Modal';
import { ModalBody } from '../modal-elements/ModalBody';
import { ModalHeader } from '../modal-elements/ModalHeader';
import { AddQuoteForm } from '../../form/quote/AddQuoteForm';
import '../styles.scss';

type QuoteModalProps = {
  clickOutsideHandler: (e: Event) => void;
  closeModal: () => void;
}

export const QuoteModal = ({ clickOutsideHandler, closeModal }: QuoteModalProps) => {
  return (
    <Modal clickOutsideHandler={ clickOutsideHandler }>
      <ModalHeader>
        <h2>Are you felling <span className="primary-color">inspired?</span></h2>
        <p>You can post one quote. You can delete it on your profile or edit in this window.</p>
      </ModalHeader>
      <ModalBody>
        <AddQuoteForm onCancelClick={closeModal} />
      </ModalBody>
    </Modal>
  );
};
