import React from 'react';

type ModalHeaderProps = {
  children: JSX.Element| JSX.Element[],
}

export const ModalHeader = ({ children }: ModalHeaderProps): JSX.Element => {
  return (
    <div className="modal-header">
      { children }
    </div>
  );
};
