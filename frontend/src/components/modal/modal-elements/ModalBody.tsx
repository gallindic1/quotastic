import React from 'react';

type ModalBodyProps = {
  children: JSX.Element| JSX.Element[],
}

export const ModalBody = ({ children }: ModalBodyProps): JSX.Element => {
  return (
    <div className="modal-body">
      { children }
    </div>
  );
};
