import React, { useRef } from 'react';
import ReactDOM from 'react-dom';
import { useOutsideAlerter } from '../../../hooks/useOutsideAlerter';

type ModalProps = {
  children: JSX.Element| JSX.Element[],
  overlay?: JSX.Element,
  clickOutsideHandler: (e: Event) => void,
};

export const Modal = ({ children, overlay, clickOutsideHandler }: ModalProps): JSX.Element => {
  const modalRef = useRef(null);
  useOutsideAlerter(modalRef, clickOutsideHandler);

  const getDOMElement = () => {
    const modalRoot = document.querySelector('.modal-root');
    return modalRoot ? modalRoot : document.body;
  };

  return ReactDOM.createPortal(
      <>
        { overlay ? overlay : null }
        <div className="modal modal-container" ref={ modalRef }>
          { children }
        </div>
      </>,
      getDOMElement(),
  );
};
