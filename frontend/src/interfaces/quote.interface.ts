interface QuoteDataInferface {
  quote: string;
};

interface RecentQuotesData {
  quote: string;
  author: string;
};

export type {
  QuoteDataInferface,
  RecentQuotesData,
};
