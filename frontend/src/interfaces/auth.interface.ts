
interface SignUpInterface {
  email: string,
  first_name: string,
  last_name: string,
  password: string,
  confirm_password: string,
};

interface LoginInterface {
  email: string,
  password: string,
};

interface UserData {
  email: string;
  first_name: string;
  last_name: string;
};

export type {
    SignUpInterface,
    LoginInterface,
    UserData,
};
