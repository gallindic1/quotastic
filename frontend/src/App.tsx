import React, { useMemo, useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { Landing } from './pages/Landing/Landing';
import { Signup } from './pages/Signup/Signup';
import { Login } from './pages/Login/Login';
import { userContext } from './contexts/userContenxt';
import { Navigation } from './components/navigation/Navigation';
import { Footer } from './components/footer/Footer';
import { UserData } from './interfaces/auth.interface';
import { authUser, getAuthToken, refreshToken, TOKEN_REFRESH_TIME } from './services/UserService';
import './assets/styles/global.scss';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  const [user, setUser] = useState<UserData | null>(null);

  useEffect(() => {
    if (!user) {
      const refreshTokenInterval = setInterval(() => refreshToken('token'), TOKEN_REFRESH_TIME);
      authenticateUser();

      return () => clearInterval(refreshTokenInterval);
    }
  }, []);

  const authenticateUser = async () => {
    const token: string | null = getAuthToken('token');

    if (!token) {
      return;
    }

    const { status, data } = await authUser(token);

    if (status === 200) {
      setUser(data);
    }
  };

  const userProvider = useMemo(
      () => ({
        user,
        setUser,
      }),
      [user, setUser],
  );

  return (
    <userContext.Provider value={userProvider}>
      <div className="app">
        <ToastContainer position="top-center" />
        <Navigation isUserLogged={ user !== null }/>
        <Routes>
          <Route path='/' element={<Landing isUserLogged={ user !== null } />} />
          <Route path='/signup' element={<Signup />} />
          <Route path='/login' element={<Login />} />
        </Routes>
        <Footer />
        <div className="modal-root"></div>
      </div>
    </userContext.Provider>
  );
};

export default App;
