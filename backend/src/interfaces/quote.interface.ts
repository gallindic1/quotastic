interface RecentQuotesData {
  quote: string;
  author: string;
}

export type { RecentQuotesData };
