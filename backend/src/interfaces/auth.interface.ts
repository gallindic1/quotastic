interface AuthData {
  token: string;
  message: string;
  user: {
    email: string;
    first_name: string;
    last_name: string;
  };
}

interface UserData {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
}

interface AuthToken {
  token: string;
}

export { AuthData, AuthToken, UserData };
