import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user-dto';
import { Repository } from 'typeorm';
import { User } from 'src/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  async findByEmail(email: string): Promise<User> {
    const user = await this.usersRepository.findOneBy({ email: email });

    if (!user) {
      throw new NotFoundException(`${email} does not exist.`);
    }

    return user;
  }

  async findById(id: number): Promise<User> {
    const user = await this.usersRepository.findOneBy({ id: id });

    if (!user) {
      throw new NotFoundException(`User does not exist.`);
    }

    return user;
  }

  public async createUser(userData: CreateUserDto): Promise<any> {
    try {
      const { email, password } = userData;
      const emailInUse = await this.userWithEmailExists(email);

      if (emailInUse) {
        throw new BadRequestException(
          `user with email: ${email} already exists.`,
        );
      }

      const hashedPassword = this.generateHashedPassword(password);

      const newUser = this.usersRepository.create({
        ...userData,
        password: hashedPassword,
      });

      await this.usersRepository.save(newUser);

      return {
        message: 'Successfully registered!',
      };
    } catch (err) {
      throw new BadRequestException([err.response.message]);
    }
  }

  private generateHashedPassword(password: string, rounds = 10): string {
    const salt = bcrypt.genSaltSync(rounds);
    return bcrypt.hashSync(password, salt);
  }

  private async userWithEmailExists(email: string): Promise<boolean> {
    const user = await this.usersRepository.findOneBy({ email: email });
    return user && email === user.getEmail();
  }
}
