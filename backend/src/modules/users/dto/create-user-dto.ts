import { IsEmail, IsNotEmpty, Matches, MinLength } from 'class-validator';
import { Match } from './match-decorator';

export class CreateUserDto {
  @IsNotEmpty({ message: 'email is required' })
  @IsEmail()
  email: string;

  @IsNotEmpty({ message: 'first name is required' })
  first_name: string;

  @IsNotEmpty({ message: 'last name is required' })
  last_name: string;

  @IsNotEmpty({ message: 'password is required' })
  @MinLength(8)
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/, {
    message: 'password too weak',
  })
  password: string;

  @IsNotEmpty({ message: 'confirming password is required' })
  @Match(CreateUserDto, (s) => s.password)
  confirm_password: string;
}
