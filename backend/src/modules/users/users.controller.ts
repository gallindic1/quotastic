import {
  Controller,
  Post,
  Get,
  Body,
  ValidationPipe,
  Request,
  UseGuards,
  Inject,
  forwardRef,
} from '@nestjs/common';
import { LocalAuthGuard } from 'src/modules/auth/local-auth.guard';
import { UsersService } from './users.service';
import { AuthService } from '../auth/auth.service';
import { CreateUserDto } from './dto/create-user-dto';
import { AuthData, AuthToken, UserData } from 'src/interfaces/auth.interface';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
  ) {}

  @Post('signup')
  async createUser(
    @Body(new ValidationPipe()) userDto: CreateUserDto,
  ): Promise<any> {
    return this.usersService.createUser(userDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req): Promise<AuthData> {
    const user = req.user;
    const token = await this.authService.login(user);

    return {
      message: 'Successsfully logged in!',
      token,
      user: {
        email: user.getEmail(),
        first_name: user.getFirstName(),
        last_name: user.getLastName(),
      },
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('auth')
  async me(@Request() req): Promise<UserData> {
    const email = req.user.email;
    const user = await this.usersService.findByEmail(email);

    return {
      id: user.getId(),
      email: user.getEmail(),
      first_name: user.getFirstName(),
      last_name: user.getLastName(),
    };
  }

  @Post('refresh-token')
  async refreshToken(@Body() body): Promise<AuthToken> {
    const token = await this.authService.refreshToken(body);

    return {
      token: token,
    };
  }
}
