import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from 'src/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findByEmail(email);
    const passwordIsCorrect = await this.isPasswordCorrect(
      password,
      user.password,
    );

    if (user && passwordIsCorrect) {
      return user;
    }

    return null;
  }

  async login(user: User): Promise<string> {
    const payload = { email: user.email, sub: user.id };

    return this.jwtService.sign(payload);
  }

  async refreshToken(payload: { name: string; sub: number }): Promise<string> {
    return this.jwtService.sign(payload);
  }

  private async isPasswordCorrect(
    password: string,
    userPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(password, userPassword);
  }
}
