import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class CreateQuoteDto {
  @IsNotEmpty({ message: 'quote is required' })
  @MinLength(5)
  @MaxLength(280)
  quote: string;
}
