import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Quote } from 'src/entities/quote.entity';
import { CreateQuoteDto } from './dto/create-quote-dto';
import { User } from 'src/entities/user.entity';
import { RecentQuotesData } from 'src/interfaces/quote.interface';

@Injectable()
export class QuotesService {
  constructor(
    @InjectRepository(Quote)
    private readonly quotesRespository: Repository<Quote>,
  ) {}

  async getRecentQuotes(): Promise<RecentQuotesData[]> {
    try {
      const quotes = await this.quotesRespository.find({
        relations: ['user'],
        order: { updated_at: 'DESC' },
      });

      return quotes.map((quote: Quote) => ({
        quote: quote.getQuote,
        author: quote.user.getFullName(),
      }));
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async createQuote(quoteDto: CreateQuoteDto, user: User): Promise<any> {
    try {
      const newQuote = this.quotesRespository.create({
        ...quoteDto,
        user,
      });

      await this.quotesRespository.save(newQuote);

      return {
        message: 'success',
      };
    } catch (err) {
      throw new BadRequestException(err);
    }
  }
}
