import {
  Controller,
  Post,
  Get,
  Body,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { CreateQuoteDto } from './dto/create-quote-dto';
import { QuotesService } from './quotes.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { User } from 'src/entities/user.entity';
import { UsersService } from '../users/users.service';
import { RecentQuotesData } from 'src/interfaces/quote.interface';

@Controller('quotes')
export class QuotesController {
  constructor(
    private readonly quotesService: QuotesService,
    private readonly usersService: UsersService,
  ) {}

  @Get('/recent')
  async getRecentQuotes(): Promise<RecentQuotesData[]> {
    return this.quotesService.getRecentQuotes();
  }

  @UseGuards(JwtAuthGuard)
  @Post('/myquote')
  async createQuote(
    @Body(new ValidationPipe()) quoteDto: CreateQuoteDto,
    @GetUser() user: User,
  ): Promise<any> {
    const usr = await this.usersService.findById(user.getId());
    return this.quotesService.createQuote(quoteDto, usr);
  }
}
