import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Quote } from './quote.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column()
  password: string;

  @Column()
  created_at: string;

  @Column()
  updated_at: string;

  @OneToMany(() => Quote, (quote) => quote.user, { onDelete: 'CASCADE' })
  quotes: Quote[];

  getId = () => this.id;

  getEmail = () => this.email;

  getFirstName = () => this.first_name;

  getLastName = () => this.last_name;

  getPassword = () => this.password;

  getCreatedAt = () => this.created_at;

  getUpdatedAt = () => this.updated_at;

  getFullName = () => this.first_name + ' ' + this.last_name;

  getQuotes = () => this.quotes;
}
