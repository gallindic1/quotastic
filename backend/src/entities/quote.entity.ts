import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('quotes')
export class Quote {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quote: string;

  @Column()
  created_at: string;

  @Column()
  updated_at: string;

  @ManyToOne(() => User, (user) => user.quotes, { onDelete: 'CASCADE' })
  user: User;

  getId = () => this.id;

  getQuote = () => this.quote;

  getCreatedAt = () => this.created_at;

  getUpdatedAt = () => this.updated_at;

  getUser = () => this.user;
}
